"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// requiresShipping only applies to gift cards
// taxable only applies to gift cards
// Some merits of returning entities wrapped in the Node class (ProductsConnection): 
// * You can separate entity data from meta like totalCount, pageInfo (for pagination), edges
//
// What determines whether an entity should be wrapped with the Node interface?
// Slugs should be generated from names, not stored?
//# sourceMappingURL=types.js.map