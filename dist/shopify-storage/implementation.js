"use strict";
// This module is responsible for combining data-fetching,
// and type adaptation into a single instance of StorageSuite,
// where data is persisted and retrieved from Shopify.
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
// Wrappers Shopify API calls
var api_calls_1 = require("./api-calls");
// TODO make adapter naming consistent
var adapters = require("./adapters");
var _a = adapters.incoming, adaptCollection = _a.adaptCollection, adaptProduct = _a.adaptProduct, adaptVariant = _a.adaptVariant, adaptShopifyImageToMedia = _a.adaptShopifyImageToMedia, adaptMetaField = _a.adaptMetaField, adaptShopifyProductTagToMetaField = _a.adaptShopifyProductTagToMetaField;
var _b = adapters.outgoing, buildProductListArgs = _b.buildProductListArgs, buildSmartCollectionListArgs = _b.buildSmartCollectionListArgs;
var ShopifyAPI = require('shopify-api-node');
//
// STORAGE SUITE IMPLEMENTATION
//
function constructStorageSuite(config) {
    var shopifyAPI = new ShopifyAPI(config === null || config === void 0 ? void 0 : config.shopifyAPI);
    return {
        Collections: constructCollectionStorage(shopifyAPI),
        CollectionMedia: constructCollectionMediaStorage(shopifyAPI),
        Products: constructProductStorage(shopifyAPI),
        Variants: constructVariantStorage(shopifyAPI),
        ProductMetaFields: constructProductMetaFieldStorage(shopifyAPI),
        VariantMetaFields: constructVariantMetaFieldStorage(shopifyAPI),
        ProductMedia: constructProductMediaStorage(shopifyAPI),
        VariantMedia: constructVariantMediaStorage(shopifyAPI),
    };
}
exports.constructStorageSuite = constructStorageSuite;
//
// COLLECTIONS
//
var constructCollectionStorage = function (shopifyAPI) { return ({
    findById: function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var shopifyCollection;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, api_calls_1.findShopifyCollectionById(shopifyAPI, id)];
                    case 1:
                        shopifyCollection = _a.sent();
                        if (!shopifyCollection)
                            return [2 /*return*/, null];
                        return [2 /*return*/, adaptCollection(shopifyCollection)];
                }
            });
        });
    },
    find: function (options) {
        if (options === void 0) { options = {}; }
        return __awaiter(this, void 0, void 0, function () {
            var args, shopifyCollections;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        args = buildSmartCollectionListArgs(options);
                        return [4 /*yield*/, api_calls_1.findShopifyCollections(shopifyAPI, args)];
                    case 1:
                        shopifyCollections = _a.sent();
                        return [2 /*return*/, Promise.all(shopifyCollections.map(adaptCollection))];
                }
            });
        });
    },
    findByProductId: function (productId, options) {
        if (options === void 0) { options = {}; }
        return __awaiter(this, void 0, void 0, function () {
            var args, shopifyCollections;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        args = buildSmartCollectionListArgs(options);
                        args = __assign(__assign({}, args), { "product_id": productId });
                        return [4 /*yield*/, api_calls_1.findShopifyCollections(shopifyAPI, args)];
                    case 1:
                        shopifyCollections = _a.sent();
                        return [4 /*yield*/, Promise.all(shopifyCollections.map(adaptCollection))];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    }
}); };
//
// PRODUCTS
//
var constructProductStorage = function (shopifyAPI) { return ({
    find: function (options) {
        if (options === void 0) { options = {}; }
        return __awaiter(this, void 0, void 0, function () {
            var args, shopifyProducts;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        args = buildProductListArgs(options);
                        return [4 /*yield*/, api_calls_1.findShopifyProducts(shopifyAPI, args)];
                    case 1:
                        shopifyProducts = _a.sent();
                        return [4 /*yield*/, Promise.all(shopifyProducts.map(adaptProduct))];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    },
    findById: function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var shopifyProduct;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, api_calls_1.findShopifyProductById(shopifyAPI, id)];
                    case 1:
                        shopifyProduct = _a.sent();
                        if (!shopifyProduct)
                            return [2 /*return*/];
                        return [2 /*return*/, adaptProduct(shopifyProduct)];
                }
            });
        });
    },
    findByCollectionId: function (collectionId) {
        return __awaiter(this, void 0, void 0, function () {
            var shopifyProducts;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, api_calls_1.findShopifyProductsByCollectionId(shopifyAPI, collectionId)];
                    case 1:
                        shopifyProducts = _a.sent();
                        return [4 /*yield*/, Promise.all(shopifyProducts.map(adaptProduct))];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    }
}); };
var constructVariantStorage = function (shopifyAPI) { return ({
    findById: function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var shopifyVariant;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, api_calls_1.findShopifyVariantById(shopifyAPI, id)];
                    case 1:
                        shopifyVariant = _a.sent();
                        if (shopifyVariant == null)
                            return [2 /*return*/, null];
                        return [2 /*return*/, adaptVariant(shopifyVariant)];
                }
            });
        });
    },
    findByProductId: function (productId) {
        return __awaiter(this, void 0, void 0, function () {
            var shopifyVariants;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, api_calls_1.findShopifyVariantsByProductId(shopifyAPI, productId)];
                    case 1:
                        shopifyVariants = _a.sent();
                        return [4 /*yield*/, Promise.all(shopifyVariants.map(adaptVariant))];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    }
}); };
//
// VARIANT METAFIELDS
//
var constructVariantMetaFieldStorage = function (shopifyAPI) { return ({
    findByVariantId: function (variantId) {
        return __awaiter(this, void 0, void 0, function () {
            var shopifyVariantMetaFields;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, api_calls_1.findShopifyVariantMetaFieldsByVariantId(shopifyAPI, variantId)];
                    case 1:
                        shopifyVariantMetaFields = _a.sent();
                        return [2 /*return*/, Promise.all(shopifyVariantMetaFields.map(adaptMetaField))];
                }
            });
        });
    },
}); };
//
// PRODUCT METAFIELDS
//
var constructProductMetaFieldStorage = function (shopifyAPI) { return ({
    findByProductId: function (productId) {
        return __awaiter(this, void 0, void 0, function () {
            var shopifyProductMetaFields, shopifyProductTags;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, api_calls_1.findShopifyProductMetaFieldsByProductId(shopifyAPI, productId)];
                    case 1:
                        shopifyProductMetaFields = _a.sent();
                        return [4 /*yield*/, api_calls_1.findShopifyProductTagsByProductId(shopifyAPI, productId)];
                    case 2:
                        shopifyProductTags = _a.sent();
                        // Combining all shopify product tags / metafields
                        return [2 /*return*/, Promise.all(__spreadArrays(shopifyProductMetaFields.map(adaptMetaField), shopifyProductTags.map(adaptShopifyProductTagToMetaField)))];
                }
            });
        });
    },
}); };
//
// COLLECTION IMAGES
//
var constructCollectionMediaStorage = function (shopifyAPI) { return ({
    findByCollectionId: function (collectionId) {
        return __awaiter(this, void 0, void 0, function () {
            var shopifyCollection;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, api_calls_1.findShopifyCollectionById(shopifyAPI, collectionId)];
                    case 1:
                        shopifyCollection = _a.sent();
                        return [2 /*return*/, shopifyCollection.image ? [
                                adaptShopifyImageToMedia(shopifyCollection.image),
                            ] : []];
                }
            });
        });
    }
}); };
//
// PRODUCT IMAGES
//
var constructProductMediaStorage = function (shopifyAPI) { return ({
    findByProductId: function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var shopifyProductImages, nonVariantImages;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, api_calls_1.findShopifyProductImagesByProductId(shopifyAPI, id)];
                    case 1:
                        shopifyProductImages = _a.sent();
                        nonVariantImages = shopifyProductImages
                            .filter(function (image) { return !image['variant_ids'].length; });
                        return [2 /*return*/, nonVariantImages.map(function (image) {
                                return adaptShopifyImageToMedia(image);
                            })];
                }
            });
        });
    }
}); };
//
// VARIANT IMAGES
//
var constructVariantMediaStorage = function (shopifyAPI) { return ({
    findByVariantId: function (variantId) {
        return __awaiter(this, void 0, void 0, function () {
            var shopifyVariant, productId, shopifyProductImages, variantImages;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, api_calls_1.findShopifyVariantById(shopifyAPI, variantId)];
                    case 1:
                        shopifyVariant = _a.sent();
                        if (!shopifyVariant)
                            return [2 /*return*/, []];
                        productId = String(shopifyVariant['product_id']);
                        return [4 /*yield*/, api_calls_1.findShopifyProductImagesByProductId(shopifyAPI, productId)];
                    case 2:
                        shopifyProductImages = _a.sent();
                        variantImages = shopifyProductImages
                            .filter(function (image) { return image['variant_ids'].includes(parseInt(variantId)); });
                        // Adapt to MediaInstance
                        return [2 /*return*/, variantImages.map(function (image) {
                                return adaptShopifyImageToMedia(image);
                            })];
                }
            });
        });
    }
}); };
//# sourceMappingURL=implementation.js.map