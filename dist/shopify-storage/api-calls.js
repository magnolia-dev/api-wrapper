"use strict";
// This module is responsible for abstracting the details
// of how to use the Shopify API to get data into the application.
//
// All necessary Shopify API calls should be represented 
// by a function in this module.
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
function findShopifyCollectionById(shopifyAPI, id) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, shopifyAPI
                    .smartCollection
                    .get(id)
                    .catch(function (e) { return undefined; })];
        });
    });
}
exports.findShopifyCollectionById = findShopifyCollectionById;
function findShopifyCollections(shopifyAPI, args) {
    if (args === void 0) { args = {}; }
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, shopifyAPI
                    .smartCollection
                    .list(args)
                    .catch(function (e) { return []; })];
        });
    });
}
exports.findShopifyCollections = findShopifyCollections;
function findShopifyProducts(shopifyAPI, args) {
    if (args === void 0) { args = {}; }
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, shopifyAPI
                    .product
                    .list(args)
                    .catch(function (e) { return []; })];
        });
    });
}
exports.findShopifyProducts = findShopifyProducts;
function findShopifyProductById(shopifyAPI, id) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, shopifyAPI
                    .product
                    .get(id)
                    .catch(function (e) { return undefined; })];
        });
    });
}
exports.findShopifyProductById = findShopifyProductById;
function findShopifyProductsByCollectionId(shopifyAPI, collectionId) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, shopifyAPI
                    .collection
                    .products(collectionId)
                    .catch(function (e) { return []; })];
        });
    });
}
exports.findShopifyProductsByCollectionId = findShopifyProductsByCollectionId;
function findShopifyProductByVariantId(shopifyAPI, variantId) {
    return __awaiter(this, void 0, void 0, function () {
        var shopifyVariant, productId;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, findShopifyVariantById(shopifyAPI, variantId)];
                case 1:
                    shopifyVariant = _a.sent();
                    if (!shopifyVariant)
                        return [2 /*return*/, undefined];
                    productId = String(shopifyVariant['product_id']);
                    return [2 /*return*/, findShopifyProductById(shopifyAPI, productId)];
            }
        });
    });
}
exports.findShopifyProductByVariantId = findShopifyProductByVariantId;
// TODO test this function
function findShopifyVariantsByProductId(shopifyAPI, productId) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, shopifyAPI
                    .productVariant
                    .list(productId)
                    .catch(function (e) { return []; })];
        });
    });
}
exports.findShopifyVariantsByProductId = findShopifyVariantsByProductId;
function findShopifyVariantById(shopifyAPI, id) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, shopifyAPI
                    .productVariant
                    .get(id)
                    .catch(function (e) { return undefined; })];
        });
    });
}
exports.findShopifyVariantById = findShopifyVariantById;
// Get product metafields from the shopify API by productId
function findShopifyProductMetaFieldsByProductId(shopifyAPI, productId) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, shopifyAPI
                    .metafield
                    .list({ metafield: { "owner_resource": "product", "owner_id": productId } })
                    .catch(function (e) { return []; })];
        });
    });
}
exports.findShopifyProductMetaFieldsByProductId = findShopifyProductMetaFieldsByProductId;
// Get product tags from the shopify API by product tag
function findShopifyProductTagsByProductId(shopifyAPI, productId) {
    var _a;
    return __awaiter(this, void 0, void 0, function () {
        var shopifyProductListing;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0: return [4 /*yield*/, shopifyAPI
                        .productListing
                        .get(productId)
                        .catch(function (e) { return undefined; })];
                case 1:
                    shopifyProductListing = _b.sent();
                    // Tags are stored as one string, separated with ", "
                    // as a property of shopify product listings.
                    // We want them as an array of strings.
                    return [2 /*return*/, ((_a = shopifyProductListing === null || shopifyProductListing === void 0 ? void 0 : shopifyProductListing.tags) !== null && _a !== void 0 ? _a : '')
                            .split(', ')
                            .filter(function (t) { return t.length; })];
            }
        });
    });
}
exports.findShopifyProductTagsByProductId = findShopifyProductTagsByProductId;
// Get variant metafields from the shopify API by variantId
function findShopifyVariantMetaFieldsByVariantId(shopifyAPI, variantId) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, shopifyAPI
                        .metafield
                        .list({ metafield: { "owner_resource": "variant", "owner_id": variantId } })
                        .catch(function (e) { return []; })];
                case 1: return [2 /*return*/, _a.sent()];
            }
        });
    });
}
exports.findShopifyVariantMetaFieldsByVariantId = findShopifyVariantMetaFieldsByVariantId;
// Get product images from the shopify API by productId
function findShopifyProductImagesByProductId(shopifyAPI, productId) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, shopifyAPI
                    .productImage
                    .list(productId)
                    .catch(function (e) { return []; })];
        });
    });
}
exports.findShopifyProductImagesByProductId = findShopifyProductImagesByProductId;
//# sourceMappingURL=api-calls.js.map