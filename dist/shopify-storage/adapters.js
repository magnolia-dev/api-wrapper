"use strict";
// This module is responsible for converting
// data types originating in Shopify to data types 
// that are native to this application, and visa-versa.
Object.defineProperty(exports, "__esModule", { value: true });
var htmlToText = require("html-to-text");
// Convert shopify data types into application native data types
exports.incoming = {
    // Transform a shopify collection into a CollectionInstance
    adaptCollection: function (shopifyCollection) {
        return {
            id: String(shopifyCollection['id']),
            title: shopifyCollection['title'],
            slug: shopifyCollection['handle'],
            isPrivate: shopifyCollection['published_at'] == null,
        };
    },
    // Transform a shopify product into a ProductInstance
    // TODO test this function
    adaptProduct: function (shopifyProduct) {
        return {
            id: String(shopifyProduct['id']),
            title: shopifyProduct['title'],
            slug: shopifyProduct['handle'],
            isPrivate: shopifyProduct['published_at'] == null,
            description: htmlToText.fromString(shopifyProduct['body_html']).replace(/\n/g, ''),
        };
    },
    // Transform a shopify product variant into a VariantInstance
    // TODO test this function
    adaptVariant: function (shopifyVariant) {
        return {
            id: String(shopifyVariant['id']),
            title: shopifyVariant['title'],
            weightInGrams: shopifyVariant['grams'],
            priceInCents: Number.parseInt(shopifyVariant['price'].replace('.', '')),
            quantityAvailable: shopifyVariant['inventory_quantity'],
        };
    },
    // Transform a Shopify product tag into a ProductMetaField
    adaptShopifyProductTagToMetaField: function (tag) {
        return {
            key: tag,
            value: "1",
            isPrivate: false,
        };
    },
    // Transform a Shopify product/variant metafield into a MetaFieldInstance
    adaptMetaField: function (shopifyMetaField) {
        // Maybe prefix keys with namespaces
        return {
            key: shopifyMetaField['key'],
            value: shopifyMetaField['value'],
            isPrivate: false,
        };
    },
    // Transform a shopify product image into a MediaInstance
    adaptShopifyImageToMedia: function (shopifyImage) {
        return {
            description: shopifyImage['alt'],
            url: shopifyImage['src'],
            format: 'jpg',
            widthInPixels: shopifyImage['width'],
            heightInPixels: shopifyImage['height'],
        };
    },
};
exports.outgoing = {
    // Translate FindOptions into an object that the shopify 
    // productList api can understand.
    // TODO test this function
    buildProductListArgs: function (options) {
        // Default args
        var args = {};
        args['published_status'] = 'published';
        // Custom args
        if (options.limit)
            args['limit'] = options.limit;
        if (options.where) {
            var _a = options.where, title = _a.title, slug = _a.slug, isPrivate = _a.isPrivate;
            if (title)
                args['title'] = title;
            if (slug)
                args['handle'] = slug;
            if (isPrivate)
                args['published_status'] = isPrivate ? 'unpublished' : 'published';
        }
        return args;
    },
    // Translate FindOptions into an object that the shopify 
    // smartCollection api can understand.
    buildSmartCollectionListArgs: function (options) {
        // Default args
        var args = {};
        args['published_status'] = 'published';
        // Custom args
        if (options.limit)
            args['limit'] = options.limit;
        if (options.where) {
            var _a = options.where, title = _a.title, slug = _a.slug, isPrivate = _a.isPrivate;
            if (title)
                args['title'] = title;
            if (slug)
                args['handle'] = slug;
            args['published_status'] = isPrivate ? 'unpublished' : 'published';
        }
        return args;
    }
};
//# sourceMappingURL=adapters.js.map