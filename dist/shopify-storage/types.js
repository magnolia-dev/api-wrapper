"use strict";
// Interfaces that represent data types from Shopify.
// The actual instances may contain extra properties,
// but these are the fields we're most concerned with.
Object.defineProperty(exports, "__esModule", { value: true });
//# sourceMappingURL=types.js.map