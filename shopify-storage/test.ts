import {
  constructStorageSuite,
} from './implementation';

// Wrappers Shopify API calls
import {
  findShopifyCollectionById,
  findShopifyCollections,
  findShopifyProducts,
  findShopifyProductById,
  findShopifyProductsByCollectionId,
  findShopifyProductByVariantId,
  findShopifyVariantsByProductId,
  findShopifyVariantById,
  findShopifyVariantMetaFieldsByVariantId,
  findShopifyProductMetaFieldsByProductId,
  findShopifyProductImagesByProductId,
  findShopifyProductTagsByProductId,
} from './api-calls'

import * as adapters from './adapters';
const { 
  adaptCollection,
  adaptProduct,
  adaptVariant,
  adaptShopifyImageToMedia,
  adaptMetaField,
  adaptShopifyProductTagToMetaField,
} = adapters.incoming;

// Create a shopify API client
const ShopifyAPI = require('shopify-api-node');
const fs = require('fs');
const json = fs.readFileSync('shopify-config.json', 'utf8');
const config = JSON.parse(json);
const shopifyAPI = new ShopifyAPI(config.shopifyAPI);

// Construct Storage Suite
const shopifyStorage = constructStorageSuite(config); 
const {
  Collections,
  CollectionMedia,
  Products,
  Variants,
  VariantMetaFields,
  ProductMetaFields,
  ProductMedia,
  VariantMedia,
} = shopifyStorage;


import * as assert from 'assert';

// Test that we're calling the shopify API correctly to get collection data
async function findShopifyCollectionByIdTest() {
  const collectionId = '80173793392'; // Kitchen and Dining
  const collection = await findShopifyCollectionById(shopifyAPI, collectionId);

  const expected = {
    id: 80173793392,
    handle: 'kitchen-dining',
    title: 'Kitchen + Dining',
    updated_at: '2020-03-10T14:50:38-05:00',
    body_html: '',
    published_at: '2018-10-05T11:49:44-05:00',
    sort_order: 'manual',
    template_suffix: '',
    products_count: 90,
    disjunctive: false,
    rules: [],
    published_scope: 'global',
    admin_graphql_api_id: 'gid://shopify/Collection/80173793392',
  };


  assert.deepEqual(
    Object.keys(collection),
    Object.keys(expected),
  );
}

// Test that we're calling the shopify API correctly to get collection data
async function findShopifyCollectionsTest() {
  const collections = await findShopifyCollections(shopifyAPI);
  
  const expected = {
    id: 80173793392,
    handle: 'kitchen-dining',
    title: 'Kitchen + Dining',
    updated_at: '2020-03-10T14:50:38-05:00',
    body_html: '',
    published_at: '2018-10-05T11:49:44-05:00',
    sort_order: 'manual',
    template_suffix: '',
    // Note: Shopify collections fetched this way don't have a 
    // `product_count` property, as they would if you fetched
    // them by id.
    disjunctive: false,
    rules: [],
    published_scope: 'global',
    admin_graphql_api_id: 'gid://shopify/Collection/80173793392',
    // Note: Shopify collections fetched this way sometimes have
    // an `image` property, and sometimes don't.
  };

  assert.equal(50, collections.length);
  collections.forEach(collection => {
    // Removing the collection image property when it exists,
    // because it interferes with our comparison.
    delete collection.image;

    assert.deepEqual(
      Object.keys(collection),
      Object.keys(expected),
    );
  });
}

// Test that we're calling the shopify API correctly to get product data
async function findShopifyProductByIdTest() {
  const productId = '4233239364';
  const shopifyProduct = await findShopifyProductById(shopifyAPI, productId);

  const expected = {
    id: 6332761412,
    title: '"Aww Shucks" Greeting Card',
    body_html: '',
    vendor: 'AH',
    product_type: 'Books + Paper',
    created_at: '2016-07-29T09:07:36-05:00',
    handle: 'aww-shucks-greeting-card',
    updated_at: '2020-03-10T15:11:49-05:00',
    published_at: null,
    template_suffix: '',
    published_scope: 'web',
    tags: 'lsob-10, standard discount collection',
    admin_graphql_api_id: 'gid://shopify/Product/6332761412',
    variants: [],
    options: [],
    images: [],
    image: {},
  };

  assert.deepEqual(
    Object.keys(shopifyProduct),
    Object.keys(expected),
  );
}

// Test that we're calling the shopify API correctly to get product data
async function findShopifyProductsTest() {
  const shopifyProducts = await findShopifyProducts(shopifyAPI);
  
  const expected = {
    id: 6332761412,
    title: '"Aww Shucks" Greeting Card',
    body_html: '',
    vendor: 'AH',
    product_type: 'Books + Paper',
    created_at: '2016-07-29T09:07:36-05:00',
    handle: 'aww-shucks-greeting-card',
    updated_at: '2020-03-10T15:11:49-05:00',
    published_at: null,
    template_suffix: '',
    published_scope: 'web',
    tags: 'lsob-10, standard discount collection',
    admin_graphql_api_id: 'gid://shopify/Product/6332761412',
    variants: [],
    options: [],
    images: [],
    image: {},
  };

  assert.equal(50, shopifyProducts.length);
  shopifyProducts.forEach(shopifyProduct => {
    assert.deepEqual(
      Object.keys(shopifyProduct),
      Object.keys(expected),
    );
  }); 
}

async function findShopifyProductsByCollectionIdTest() {
  const collectionId = '80173793392';
  const shopifyProducts =
    await findShopifyProductsByCollectionId(shopifyAPI, collectionId);
  
  const expected = {
    id: 390965133316,
    title: 'Magnolia Established Plate',
    body_html: '',
    vendor: 'SF',
    product_type: 'MT Table Top',
    created_at: '2018-02-16T09:09:03-06:00',
    handle: 'magnolia-established-plate',
    updated_at: '2020-03-10T14:47:44-05:00',
    published_at: '2019-09-27T15:08:49-05:00',
    template_suffix: '',
    published_scope: 'global',
    tags: '',
    // Note: Shopify products fetched this way don't include
    // a `variants` property.
    admin_graphql_api_id: 'gid://shopify/Product/390965133316',
    options: [],
    images: [],
    image: {},
  };

  assert(shopifyProducts.length);
  shopifyProducts.forEach(shopifyProduct => {
    assert.deepEqual(
      Object.keys(shopifyProduct),
      Object.keys(expected),
    );
  }); 
}

async function findShopifyProductByVariantIdTest() {
  const variantId = '861172421'; // Today Is A Good Day Sign

  const shopifyProduct =
    await findShopifyProductByVariantId(shopifyAPI, variantId);

  const expected = {
    id: 6332761412,
    title: '"Aww Shucks" Greeting Card',
    body_html: '',
    vendor: 'AH',
    product_type: 'Books + Paper',
    created_at: '2016-07-29T09:07:36-05:00',
    handle: 'aww-shucks-greeting-card',
    updated_at: '2020-03-10T15:11:49-05:00',
    published_at: null,
    template_suffix: '',
    published_scope: 'web',
    tags: 'lsob-10, standard discount collection',
    admin_graphql_api_id: 'gid://shopify/Product/6332761412',
    variants: [],
    options: [],
    images: [],
    image: {},
  };

  assert.deepEqual(
    Object.keys(shopifyProduct),
    Object.keys(expected),
  );
}

async function findShopifyVariantByIdTest() {
  const variantId = '861172421'; // Today Is A Good Day Sign

  const shopifyVariant =
    await findShopifyVariantById(shopifyAPI, variantId);

  const expected = {
    id: 861172421,
    product_id: 348775965,
    title: 'Large',
    price: '88.00',
    sku: 'JDTODAY15L',
    position: 1,
    inventory_policy: 'deny',
    compare_at_price: null,
    fulfillment_service: 'manual',
    inventory_management: 'shopify',
    option1: 'Large',
    option2: null,
    option3: null,
    created_at: '2014-07-23T08:05:55-05:00',
    updated_at: '2020-03-09T23:21:09-05:00',
    taxable: true,
    barcode: '132017111528',
    grams: 6350,
    image_id: 4979890323507,
    weight: 6350,
    weight_unit: 'g',
    inventory_item_id: 13922226052,
    inventory_quantity: 120,
    old_inventory_quantity: 120,
    tax_code: '',
    requires_shipping: true,
    admin_graphql_api_id: 'gid://shopify/ProductVariant/861172421'
  };

  assert.deepEqual(
    Object.keys(shopifyVariant),
    Object.keys(expected),
  );
}

async function collectionsFindByIdTest() {
  const collection = await Collections 
    .findById('80173793392');

  const expected = {
    id: '80173793392',
    title: 'Kitchen + Dining',
    slug: 'kitchen-dining',
    isPrivate: false
  }

  // Values will change, but keys should be consistent
  assert.deepEqual(
    Object.keys(collection), 
    Object.keys(expected),
  );
}

async function collectionsFindLimitTest() {
  const collections = await Collections.find({
     limit: 1
   });
  assert.equal(1, collections.length);
}

async function collectionsFindWhereTitleTest() {
  const collections = await Collections.find({
    where: { title: "Kitchen + Dining" },
  });

  const expected = [
    {
      id: '80173793392',
      title: 'Kitchen + Dining',
      slug: 'kitchen-dining',
      isPrivate: false
    }
  ];  
  assert.deepEqual(expected, collections);
}

async function collectionsFindWhereSlugTest() {
  const collections = await Collections.find({
      where: { slug: 'chips-corner' }
   });
  const expected = [
    {
      id: '179472772',
      title: 'Chip\'s Corner',
      slug: 'chips-corner',
      isPrivate: false
    }
  ];  
  assert.deepEqual(expected, collections);
}

async function collectionsFindWhereIsPrivateTrueTest() {
  const collections = await Collections.find({
     where: { isPrivate: true },
     limit: 10,
  });
  assert.equal(collections.length, 10);
  collections.forEach(c => assert(c.isPrivate));
}

async function collectionsFindWhereIsPrivateFalseTest() {
  const collections = await Collections.find({
     where: { isPrivate: false },
     limit: 10,
  });
  assert.equal(collections.length, 10);
  collections.forEach(c => {
    assert.equal(c.isPrivate, false);
  });
}

async function collectionsFindWhereIsPrivateDefaultTest() {
  // Only non private collections should be retrieved by default
  const collections = await Collections.find({
     limit: 10,
  });
  assert.equal(collections.length, 10);
  collections.forEach(c => {
    assert.equal(c.isPrivate, false);
  });
}

async function collectionsFindByProductIdTest() {
  const springCandleId = "4233239364";
  const collections = await Collections.findByProductId(springCandleId, {
    limit: 1
  });

  assert.equal(collections.length, 1);
}

async function collectionsFindByNonExistentProductIdTest() {
  const nonExistentProductId = "000000000";
  const collections = await Collections.findByProductId(nonExistentProductId);

  assert.equal(collections.length, 0);
}

async function productsFindByIdTest() {
  const product = await Products 
    .findById('333079379972');

  const expected = {
    id: '333079379972',
    title: 'Camila Terra Cotta Pot',
    slug: 'camila-terra-cotta-pot',
    isPrivate: false,
    description: 'Clean, simple lines make our Camila Terra Cotta Pot an easy fit with any style.These exclusive planters are embossed with a logo reading, “Magnolia est. 2003”and include a drainage hole and saucer. Available in three sizes.',
  };

  // Values will change, but keys should be consistent
  assert.deepEqual(
    Object.keys(product), 
    Object.keys(expected),
  );
}

async function productsFindLimitTest() {
  const products = await Products.find({
    limit: 1
  });
  assert.equal(1, products.length);
}

async function productsFindByCollectionIdTest() {
  const products = await Products
    .findByCollectionId('172852676'); // Wall Decor
  assert(products.length);
}

async function productsFindByNonExistentCollectionIdTest() {
  const nonExistentCollectionId = "000000000";
  const products= await Products.findByCollectionId(nonExistentCollectionId);
  assert.equal(products.length, 0);
}

async function productsFindWhereTitleTest() {
  const products = await Products.find({
    where: { title: "Camila Terra Cotta Pot" },
  });
  const expected = [{
    id: '333079379972',
    title: 'Camila Terra Cotta Pot',
    slug: 'camila-terra-cotta-pot',
    isPrivate: false,
    description: 'Clean, simple lines make our Camila Terra Cotta Pot an easy fit with any style.These exclusive planters are embossed with a logo reading, “Magnolia est. 2003”and include a drainage hole and saucer. Available in three sizes.',
  }];
  assert.deepEqual(products, expected);
}

async function productsFindWhereSlugTest() {
  const products = await Products.find({
    where: { slug: "camila-terra-cotta-pot" },
  });
  const expected = [{
    id: '333079379972',
    title: 'Camila Terra Cotta Pot',
    slug: 'camila-terra-cotta-pot',
    isPrivate: false,
    description: 'Clean, simple lines make our Camila Terra Cotta Pot an easy fit with any style.These exclusive planters are embossed with a logo reading, “Magnolia est. 2003”and include a drainage hole and saucer. Available in three sizes.',
  }];
  assert.deepEqual(products, expected);
}

async function productsFindWhereIsPrivateTrueTest() {
  const products = await Products.find({
     where: { isPrivate: true },
     limit: 10,
  });
  assert.equal(products.length, 10);
  products.forEach(p => assert(p.isPrivate));
}

async function productsFindWhereIsPrivateFalseTest() {
  const products = await Products.find({
     where: { isPrivate: false },
     limit: 10,
  });
  assert.equal(products.length, 10);
  products.forEach(p => assert(!p.isPrivate));
}

async function productsFindWhereIsPrivateDefaultTest() {
  const products = await Products.find({
     limit: 10,
  });
  assert.equal(products.length, 10);
  products.forEach(p => assert(!p.isPrivate));
}

// Verify that we're using the shopify API correctly to find
// variant metafields
async function findShopifyVariantMetaFieldsByVariantIdTest() {
  const variantId = '861172421'; // Today Is A Good Day Sign
  const shopifyVariantMetaFields =
    await findShopifyVariantMetaFieldsByVariantId(shopifyAPI, variantId);

  // We want to make sure we're testing a variant with metafields
  assert(shopifyVariantMetaFields.length);

  shopifyVariantMetaFields.forEach(metaField => {
    assert.equal(metaField['owner_resource'], 'variant');
    assert.equal(metaField['owner_id'], parseInt(variantId));
  });
}

// Make sure that shopify variant metafields are correctly adapted
// to our application native equivalent
function adaptVariantMetaFieldTest() {
  const input = {
    id: 10874015186995,
    namespace: 'netsuite',
    key: 'dropship-item',
    value: 'false',
    value_type: 'string',
    description: null,
    owner_id: 1181204152324,
    created_at: '2019-10-01T20:58:54-05:00',
    updated_at: '2019-10-01T20:58:54-05:00',
    owner_resource: 'variant',
    admin_graphql_api_id: 'gid://shopify/Metafield/10874015186995'
  };

  const output = adaptMetaField(input);
  assert.deepEqual(output, {
    key: 'dropship-item',
    value: 'false',
    isPrivate: false,
  });
}

async function variantsFindByProductIdTest() {
  const variants = await Variants 
    .findByProductId('391184973828'); // Magnolia Signature Candle Collection

  assert(variants.length);
}

async function variantsFindByIdTest() {
  const variant = await Variants 
    .findById('1181204152324');

  const expected = {
    id: '1181204152324',
    title: '26 oz / No. 1 Linen',
    weightInGrams: 907,
    priceInCents: 5800,
    quantityAvailable: 2069
  };

  // Values will change, but keys should be consistent
  assert.deepEqual(
    Object.keys(variant), 
    Object.keys(expected),
  );
}

async function variantMetaFieldsFindByVariantIdTest() {
  const variantMetaFields = await VariantMetaFields
    .findByVariantId('1181204152324');

  assert(variantMetaFields.length);
}

// Verify that we're using the shopify API correctly to find
// product metafields
async function findShopifyProductMetaFieldsByProductIdTest() {
  const productId = '1589542355056'; // Joanna's Favorite Mini Crossbody
  const shopifyProductMetaFields =
    await findShopifyProductMetaFieldsByProductId(shopifyAPI, productId);

  // We want to make sure we're testing a product with metafields
  assert(shopifyProductMetaFields.length);

  shopifyProductMetaFields.forEach(metaField => {
    assert.equal(metaField['owner_resource'], 'product');
    assert.equal(metaField['owner_id'], parseInt(productId));
  });
}

// Verify that we're using the shopify API correctly to find
// product tags
async function findShopifyProductTagsByProductIdTest() {
  const productId = '1589542355056'; // Joanna's Favorite Mini Crossbody
  const shopifyProductTags =
    await findShopifyProductTagsByProductId(shopifyAPI, productId);

  // We want to make sure we're testing a product with tags
  assert(shopifyProductTags.length);

  // All tags should be non-empty strings
  shopifyProductTags.forEach(tag => assert(tag.length));
}

// Make sure that shopify product metafields are correctly adapted
// to our application native equivalent
function adaptProductMetaFieldTest() {
  const input = {
    id: 5103533293619,
    namespace: 'global',
    key: 'title_tag',
    value: "Joanna's Favorite Mini Crossbody | Magnolia",
    value_type: 'string',
    description: null,
    owner_id: 1589542355056,
    created_at: '2019-03-05T13:38:23-06:00',
    updated_at: '2019-08-07T19:21:37-05:00',
    owner_resource: 'product',
    admin_graphql_api_id: 'gid://shopify/Metafield/5103533293619'
  };

  const output = adaptMetaField(input);
  assert.deepEqual(output, {
    key: 'title_tag',
    value: "Joanna's Favorite Mini Crossbody | Magnolia",
    isPrivate: false,
  });
}

// Make sure that shopify product tags are correctly adapted
// to our application native metafields
function adaptShopifyProductTagToMetaFieldTest() {
  const input = 'best-seller';

  const output = adaptShopifyProductTagToMetaField(input);
  assert.deepEqual(output, {
    key: 'best-seller',
    value: "1",
    isPrivate: false,
  });
}

async function productMetaFieldsFindByProductIdTest() {
  const shopifyProductMetaFields = await ProductMetaFields 
    .findByProductId('1589542355056');
}

async function findShopifyProductImagesByProductIdTest() {
  const productId = '1589542355056';
  const shopifyProductImages =
    await findShopifyProductImagesByProductId(shopifyAPI, productId);

  const exampleImage = {
    id: 11354631372851,
    product_id: 1589542355056,
    position: 1,
    created_at: '2019-06-21T13:22:36-05:00',
    updated_at: '2019-06-21T13:23:57-05:00',
    alt: 'small brown leather crossbody purse with herringbone pattern stitching',
    width: 2048,
    height: 1024,
    src: 'https://cdn.shopify.com/s/files/1/0207/8508/products/joannas-favorite-mini-crossbody-MH0088.jpg?v=1561141437',
    variant_ids: [],
    admin_graphql_api_id: 'gid://shopify/ProductImage/11354631372851'
  };

  // Verify that we're testing with a product that has images
  assert(shopifyProductImages.length);

  // Each product image must have the correct fields
  shopifyProductImages.forEach(image => {
    assert.deepEqual(
      Object.keys(image),
      Object.keys(exampleImage),
    );
  });

}

// Make sure that shopify images are correctly adapted
// to our application native MediaInstance
function adaptShopifyImageToMediaTest() {
  const input = {
    id: 11354631372851,
    product_id: 1589542355056,
    position: 1,
    created_at: '2019-06-21T13:22:36-05:00',
    updated_at: '2019-06-21T13:23:57-05:00',
    alt: 'small brown leather crossbody purse with herringbone pattern stitching',
    width: 2048,
    height: 1024,
    src: 'https://cdn.shopify.com/s/files/1/0207/8508/products/joannas-favorite-mini-crossbody-MH0088.jpg?v=1561141437',
    variant_ids: [],
    admin_graphql_api_id: 'gid://shopify/ProductImage/11354631372851'
  };

  const output = adaptShopifyImageToMedia(input);

  assert.deepEqual(output, {
    description: 'small brown leather crossbody purse with herringbone pattern stitching',
    url: 'https://cdn.shopify.com/s/files/1/0207/8508/products/joannas-favorite-mini-crossbody-MH0088.jpg?v=1561141437',
    format: 'jpg',
    widthInPixels: 2048,
    heightInPixels: 1024,
  });
}

async function productMediaExcludesVariantImagesTest() {
  const productId = '4233239364'; // Magnolia Spring Candle
  const shopifyProductImages = 
    await findShopifyProductImagesByProductId(shopifyAPI, productId);

  const productMedia = await ProductMedia
    .findByProductId(productId);

  assert(productMedia.length < shopifyProductImages.length);
}

async function variantMediaExcludesProductImagesTest() {
  const productId = '4233239364'; // Magnolia Spring Candle
  const shopifyProductImages = 
    await findShopifyProductImagesByProductId(shopifyAPI, productId);

  const productMedia = await ProductMedia
    .findByProductId(productId);

  assert(productMedia.length < shopifyProductImages.length);
}

async function collectionMediaFindByCollectionIdTest() {
  const collectionId = '1391558660';
  const collectionMedia = await
    CollectionMedia.findByCollectionId(collectionId);

  const expected = [{
    description: '12 days of christmas sale',
    url: 'https://cdn.shopify.com/s/files/1/0207/8508/collections/18_12-Days_Collection_Header.jpg?v=1543611746',
    format: 'jpg',
    widthInPixels: 2040,
    heightInPixels: 510
  }];

  assert.deepEqual(collectionMedia, expected);
}


// Instead of being invoked individually, tests 
// are wrapped below in functions that represent
// a broader category that they belong to.
// This allows test categories to be run in isolation.


// Tests for functions that handle the details
// of fetching data from shopify.
function apiCallsTest() {
  findShopifyCollectionByIdTest();
  findShopifyCollectionsTest();
  findShopifyProductsTest();
  findShopifyProductByIdTest();
  findShopifyProductsByCollectionIdTest();
  findShopifyProductByVariantIdTest();
  findShopifyProductTagsByProductIdTest();
  findShopifyProductImagesByProductIdTest();
  findShopifyVariantByIdTest();
  findShopifyVariantMetaFieldsByVariantIdTest();
  findShopifyProductMetaFieldsByProductIdTest();
}

// Tests for converting between shopify data types,
// and data types native to this application.
function adapterTests() {
  adaptVariantMetaFieldTest
  adaptProductMetaFieldTest();
  adaptShopifyProductTagToMetaFieldTest();
  adaptShopifyImageToMediaTest();
}

// Tests for functions that orchestrate
// data-fetching functions, and adaptation functions
// to deliver application native data types that are
// persisted outside the application.
function storageDeviceTests() {
  collectionsFindByIdTest();
  collectionsFindLimitTest();
  collectionsFindWhereTitleTest();
  collectionsFindWhereSlugTest();
  collectionsFindWhereIsPrivateTrueTest();
  collectionsFindWhereIsPrivateFalseTest();
  collectionsFindWhereIsPrivateDefaultTest();
  collectionsFindByProductIdTest();
  collectionsFindByNonExistentProductIdTest();
  collectionMediaFindByCollectionIdTest();
  productsFindByIdTest();
  productsFindLimitTest();
  productsFindByCollectionIdTest();
  productsFindByNonExistentCollectionIdTest();
  productsFindWhereTitleTest();
  productsFindWhereSlugTest();
  productsFindWhereIsPrivateTrueTest();
  productsFindWhereIsPrivateFalseTest();
  productsFindWhereIsPrivateDefaultTest();
  variantsFindByProductIdTest();
  variantsFindByIdTest();
  variantMetaFieldsFindByVariantIdTest();
  productMetaFieldsFindByProductIdTest();
  productMediaExcludesVariantImagesTest();
}

// Execute test categories
(() => {
  if (!process.env.SKIP_API_CALL_TESTS) {
    apiCallsTest();
  }
  if (!process.env.SKIP_ADAPTER_TESTS) {
    adapterTests();
  }
  if (!process.env.SKIP_STORAGE_DEVICE_TESTS) {
    storageDeviceTests();
  }
})();

