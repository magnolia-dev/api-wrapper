
// This module is responsible for combining data-fetching,
// and type adaptation into a single instance of StorageSuite,
// where data is persisted and retrieved from Shopify.

// Native Types
import {
  StorageSuite,
  StorageSuiteConfig,
  StorageDevice,
  CollectionStorage,
  CollectionMediaStorage,
  ProductStorage,
  ProductMetaFieldStorage,
  ProductMediaStorage,
  VariantStorage,
  VariantMetaFieldStorage,
  VariantMediaStorage,
  FindOptions,
  CollectionInstance,
  ProductInstance,
  VariantInstance,
  MetaFieldInstance,
  MediaInstance,
} from '../types';

// Shopify Types
import {
  ShopifyCollection,
  ShopifyProduct,
  ShopifyVariant,
  ShopifyMetaField,
  ShopifyProductImage,
} from './types'

// Wrappers Shopify API calls
import {
  findShopifyCollectionById,
  findShopifyCollections,
  findShopifyProducts,
  findShopifyProductById,
  findShopifyProductsByCollectionId,
  findShopifyProductByVariantId,
  findShopifyVariantsByProductId,
  findShopifyVariantById,
  findShopifyVariantMetaFieldsByVariantId,
  findShopifyProductMetaFieldsByProductId,
  findShopifyProductImagesByProductId,
  findShopifyProductTagsByProductId,
} from './api-calls'

// TODO make adapter naming consistent
import * as adapters from './adapters';
const { 
  adaptCollection,
  adaptProduct,
  adaptVariant,
  adaptShopifyImageToMedia,
  adaptMetaField,
  adaptShopifyProductTagToMetaField,
} = adapters.incoming;
const { 
  buildProductListArgs,
  buildSmartCollectionListArgs,
} = adapters.outgoing;


const ShopifyAPI = require('shopify-api-node');

//
// STORAGE SUITE IMPLEMENTATION
//

export function constructStorageSuite(config: StorageSuiteConfig): StorageSuite {
  const shopifyAPI = new ShopifyAPI(config?.shopifyAPI);
  return {
    Collections: constructCollectionStorage(shopifyAPI),
    CollectionMedia: constructCollectionMediaStorage(shopifyAPI),
    Products: constructProductStorage(shopifyAPI),
    Variants: constructVariantStorage(shopifyAPI),
    ProductMetaFields: constructProductMetaFieldStorage(shopifyAPI),
    VariantMetaFields: constructVariantMetaFieldStorage(shopifyAPI),
    ProductMedia: constructProductMediaStorage(shopifyAPI),
    VariantMedia: constructVariantMediaStorage(shopifyAPI),
  }
}

//
// COLLECTIONS
//

const constructCollectionStorage: (shopifyAPI:any) => CollectionStorage = shopifyAPI => ({
  async findById(id:string) {
    const shopifyCollection = await findShopifyCollectionById(shopifyAPI, id);
    if (!shopifyCollection) return null;
    return adaptCollection(shopifyCollection);
  },
  async find(options:FindOptions={}): Promise<CollectionInstance[]> {
    let args = buildSmartCollectionListArgs(options);
    const shopifyCollections = await findShopifyCollections(shopifyAPI, args);
    return Promise.all(shopifyCollections.map(adaptCollection));
  },
  async findByProductId(productId:string, options:FindOptions={}): Promise<CollectionInstance[]> {
    let args = buildSmartCollectionListArgs(options);
    args = { ...args, "product_id": productId };
    const shopifyCollections = await findShopifyCollections(shopifyAPI, args);
    return await Promise.all(shopifyCollections.map(adaptCollection));
  }
})

//
// PRODUCTS
//

const constructProductStorage: (shopifyAPI:any) => ProductStorage = shopifyAPI => ({
  async find(options:FindOptions={}): Promise<ProductInstance[]> {
    const args = buildProductListArgs(options);
    const shopifyProducts = await findShopifyProducts(shopifyAPI, args);
    return await Promise.all(shopifyProducts.map(adaptProduct));
  },
  async findById(id:string): Promise<ProductInstance|undefined> {
    const shopifyProduct = await findShopifyProductById(shopifyAPI, id);
    if (!shopifyProduct) return;
    return adaptProduct(shopifyProduct);
  },
  async findByCollectionId(collectionId:string) {
    const shopifyProducts =
      await findShopifyProductsByCollectionId(shopifyAPI, collectionId);
    return await Promise.all(shopifyProducts.map(adaptProduct));
  }
})

const constructVariantStorage = shopifyAPI => ({
  async findById(id:string) {
    const shopifyVariant = await findShopifyVariantById(shopifyAPI, id);

    if (shopifyVariant == null) return null;
    return adaptVariant(shopifyVariant);
  },
  async findByProductId(productId:string) {
    const shopifyVariants = await findShopifyVariantsByProductId(shopifyAPI, productId);
    return await Promise.all(shopifyVariants.map(adaptVariant));
  }
})

//
// VARIANT METAFIELDS
//

const constructVariantMetaFieldStorage = shopifyAPI => ({
  async findByVariantId(variantId:string): Promise<MetaFieldInstance[]>  {
    const shopifyVariantMetaFields =
       await findShopifyVariantMetaFieldsByVariantId(shopifyAPI, variantId);

    return Promise.all(shopifyVariantMetaFields.map(adaptMetaField));
  },
})

//
// PRODUCT METAFIELDS
//

const constructProductMetaFieldStorage = shopifyAPI => ({
  async findByProductId(productId:string) {

    const shopifyProductMetaFields =
      await findShopifyProductMetaFieldsByProductId(shopifyAPI, productId);

    const shopifyProductTags =
      await findShopifyProductTagsByProductId(shopifyAPI, productId);

    // Combining all shopify product tags / metafields
    return Promise.all([
      ...shopifyProductMetaFields.map(adaptMetaField),
      ...shopifyProductTags.map(adaptShopifyProductTagToMetaField),
    ]);
  },
})

//
// COLLECTION IMAGES
//

// TODO Test is empty array for non-existent collection ids
const constructCollectionMediaStorage = shopifyAPI => ({
  async findByCollectionId(collectionId:string): Promise<MediaInstance[]> {
    const shopifyCollection = 
      await findShopifyCollectionById(shopifyAPI, collectionId);

    return shopifyCollection?.image ? [
      adaptShopifyImageToMedia(shopifyCollection.image),
    ] : [];
  }
})

//
// PRODUCT IMAGES
//

const constructProductMediaStorage = shopifyAPI => ({
  async findByProductId(id:string): Promise<MediaInstance[]> {
    const shopifyProductImages = 
      await findShopifyProductImagesByProductId(shopifyAPI, id);

    // Filter out variant images
    const nonVariantImages = shopifyProductImages
      .filter(image => !image['variant_ids'].length);

    return nonVariantImages.map(image => {
      return adaptShopifyImageToMedia(image);
    });
  }
})

//
// VARIANT IMAGES
//

const constructVariantMediaStorage = shopifyAPI => ({
  async findByVariantId(variantId:string): Promise<MediaInstance[]> {
    // Get Variant
    const shopifyVariant = await findShopifyVariantById(shopifyAPI, variantId);
    if (!shopifyVariant) return [];
  
    // Get Product Images
    const productId = String(shopifyVariant['product_id']);
    const shopifyProductImages =
      await findShopifyProductImagesByProductId(shopifyAPI, productId);

    // Filter out non-variant images
    const variantImages = shopifyProductImages
      .filter(image => image['variant_ids'].includes(parseInt(variantId)));

    // Adapt to MediaInstance
    return variantImages.map(image => {
      return adaptShopifyImageToMedia(image);
    });
  }
});

