
// Interfaces that represent data types from Shopify.
// The actual instances may contain extra properties,
// but these are the fields we're most concerned with.

export interface ShopifyCollection {
  id: number,
  handle: string,
  title: string,
  published_at: string,
  image?: ShopifyCollectionImage,
}

export interface ShopifyCollectionImage {
  created_at: string,
  alt: string,
  width: number,
  height: number,
  src: string,
}

export interface ShopifyProduct {
  id: number,
  title: string,
  handle: string,
  published_at: string,
  body_html: string,
  tags: string,
}

export interface ShopifyVariant {
  product_id: number,
  title: string,
  handle: string,
  grams: number,
}

export interface ShopifyMetaField {
  id: number,
  namespace: string,
  key: string,
  value: string,
  value_type: string,
  description: null,
  owner_id: number,
  created_at: string,
  updated_at: string,
  owner_resource: string,
  admin_graphql_api_id: string,
}

export interface ShopifyProductImage {
  id: number,
  product_id: number,
  position: number,
  created_at: string,
  updated_at: string,
  alt: string,
  width: number,
  height: number,
  src: string,
  variant_ids: number[],
  admin_graphql_api_id: string,
}

