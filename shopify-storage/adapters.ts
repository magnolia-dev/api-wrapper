
// This module is responsible for converting
// data types originating in Shopify to data types 
// that are native to this application, and visa-versa.

// Native Types
import {
  StorageSuite,
  StorageSuiteConfig,
  StorageDevice,
  CollectionStorage,
  ProductStorage,
  ProductMetaFieldStorage,
  ProductMediaStorage,
  VariantStorage,
  VariantMetaFieldStorage,
  VariantMediaStorage,
  FindOptions,
  CollectionInstance,
  ProductInstance,
  VariantInstance,
  MetaFieldInstance,
  MediaInstance,
} from '../types';

// Shopify Types
import {
  ShopifyCollection,
  ShopifyCollectionImage,
  ShopifyProduct,
  ShopifyVariant,
  ShopifyMetaField,
  ShopifyProductImage,
} from './types'

import * as htmlToText from 'html-to-text';

// Convert shopify data types into application native data types
export const incoming = {
  // Transform a shopify collection into a CollectionInstance
  adaptCollection(
    shopifyCollection: ShopifyCollection
  ): CollectionInstance {
    return {
      id: String(shopifyCollection['id']),
      title: shopifyCollection['title'],
      slug: shopifyCollection['handle'],
      isPrivate: shopifyCollection['published_at'] == null,
    }
  },
  // Transform a shopify product into a ProductInstance
  // TODO test this function
  adaptProduct(
    shopifyProduct: ShopifyProduct
  ): ProductInstance {
    return {
      id: String(shopifyProduct['id']),
      title: shopifyProduct['title'],
      slug: shopifyProduct['handle'],
      isPrivate: shopifyProduct['published_at'] == null,
      description: shopifyProduct['body_html'],
    }
  },

  // Transform a shopify product variant into a VariantInstance
  // TODO test this function
  adaptVariant(
    shopifyVariant: ShopifyVariant,
  ): VariantInstance {
    return {
      id: String(shopifyVariant['id']),
      title: shopifyVariant['title'],
      weightInGrams: shopifyVariant['grams'],
      priceInCents: Number.parseInt(shopifyVariant['price'].replace('.','')),
      quantityAvailable: shopifyVariant['inventory_quantity'],
    }
  },

  // Transform a Shopify product tag into a ProductMetaField
  adaptShopifyProductTagToMetaField(
    tag:string
  ): MetaFieldInstance {
    return {
      key: tag,
      value: "1",
      isPrivate: false,
    }
  },

  // Transform a Shopify product/variant metafield into a MetaFieldInstance
  adaptMetaField(
    shopifyMetaField: ShopifyMetaField,
  ): MetaFieldInstance {
    // Maybe prefix keys with namespaces
    return {
      key: shopifyMetaField['key'],
      value: shopifyMetaField['value'],
      isPrivate: false,
    }
  },

  // Transform a shopify product image into a MediaInstance
  // TODO test defaults on objects without expected properties
  // TODO should width/height be nullable?
  adaptShopifyImageToMedia(
    shopifyImage: ShopifyProductImage|ShopifyCollectionImage,
  ): MediaInstance {
    return {
      description: shopifyImage['alt'] ?? '',
      url: shopifyImage['src'] ?? '',
      format: 'jpg',
      widthInPixels: shopifyImage['width'] ?? 0,
      heightInPixels: shopifyImage['height'] ?? 0,
    }
  },
};

export const outgoing = {
  // Translate FindOptions into an object that the shopify 
  // productList api can understand.
  // TODO test this function
  buildProductListArgs(options:FindOptions) {
    // Default args
    const args = {}
    args['published_status'] = 'published';
  
    // Custom args
    if (options.limit) args['limit'] = options.limit;
    if (options.where) {
      const { title, slug, isPrivate } = options.where;
      if (title) args['title'] = title;
      if (slug) args['handle'] = slug;
      if (isPrivate) args['published_status'] = isPrivate ? 'unpublished' : 'published';
    }
    return args;
  },
  // Translate FindOptions into an object that the shopify 
  // smartCollection api can understand.
  buildSmartCollectionListArgs(options:FindOptions) {
    // Default args
    const args = {}
    args['published_status'] = 'published';
  
    // Custom args
    if (options.limit) args['limit'] = options.limit;
    if (options.where) {
      const { title, slug, isPrivate } = options.where;
      if (title) args['title'] = title;
      if (slug) args['handle'] = slug;
      args['published_status'] = isPrivate ? 'unpublished' : 'published';
    }
    return args;
  }
}


