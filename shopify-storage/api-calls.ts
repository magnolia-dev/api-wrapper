
// This module is responsible for abstracting the details
// of how to use the Shopify API to get data into the application.
//
// All necessary Shopify API calls should be represented 
// by a function in this module.

// Shopify Types
import {
  ShopifyCollection,
  ShopifyProduct,
  ShopifyVariant,
  ShopifyMetaField,
  ShopifyProductImage,
} from './types'

export async function findShopifyCollectionById(
  shopifyAPI,
  id:string
): Promise<ShopifyCollection|undefined> {
  return shopifyAPI
    .smartCollection
    .get(id)
    .catch(e => undefined);
}

export async function findShopifyCollections(
  shopifyAPI,
  args = {},
): Promise<ShopifyCollection[]> {
  return shopifyAPI
    .smartCollection
    .list(args)
    .catch(e => []);
}

export async function findShopifyProducts(
  shopifyAPI,
  args = {},
): Promise<ShopifyProduct[]> {
  return shopifyAPI
    .product
    .list(args)
    .catch(e => []);
}

export async function findShopifyProductById(
  shopifyAPI,
  id:string
): Promise<ShopifyProduct|undefined> {
  return shopifyAPI
    .product
    .get(id)
    .catch(e => undefined);
}

export async function findShopifyProductsByCollectionId(
  shopifyAPI,
  collectionId:string
): Promise<ShopifyProduct[]> {
  return shopifyAPI
    .collection
    .products(collectionId)
    .catch(e => []);
}

export async function findShopifyProductByVariantId(
  shopifyAPI,
  variantId:string
): Promise<ShopifyProduct|undefined> {
  const shopifyVariant = await findShopifyVariantById(shopifyAPI, variantId);
  if (!shopifyVariant) return undefined;

  const productId = String(shopifyVariant['product_id']);
  return findShopifyProductById(shopifyAPI, productId);
}

// TODO test this function
export async function findShopifyVariantsByProductId(
  shopifyAPI,
  productId:string
): Promise<ShopifyVariant[]> {
  return shopifyAPI
    .productVariant
    .list(productId)
    .catch(e => []);
}

export async function findShopifyVariantById(
  shopifyAPI,
  id:string
): Promise<ShopifyVariant|undefined> {
  return shopifyAPI
    .productVariant
    .get(id)
    .catch(e => undefined);
}

// Get product metafields from the shopify API by productId
export async function findShopifyProductMetaFieldsByProductId(
  shopifyAPI,
  productId:string
): Promise<ShopifyMetaField[]> {
  return shopifyAPI
    .metafield
    .list({ metafield: { "owner_resource": "product", "owner_id": productId} })
    .catch(e => []);
}

// Get product tags from the shopify API by product tag
export async function findShopifyProductTagsByProductId(
  shopifyAPI,
  productId:string
): Promise<string[]> {
  const shopifyProductListing = await shopifyAPI
    .productListing
    .get(productId)
    .catch(e => undefined);

  // Tags are stored as one string, separated with ", "
  // as a property of shopify product listings.
  // We want them as an array of strings.
  return (shopifyProductListing?.tags ?? '')
    .split(', ')
    .filter(t => t.length);
}

// Get variant metafields from the shopify API by variantId
export async function findShopifyVariantMetaFieldsByVariantId(
  shopifyAPI,
  variantId:string
) {
  return await shopifyAPI
    .metafield
    .list({ metafield: { "owner_resource": "variant", "owner_id": variantId } })
    .catch(e => []);
}

// Get product images from the shopify API by productId
export async function findShopifyProductImagesByProductId(
  shopifyAPI,
  productId:string
): Promise<ShopifyProductImage[]> {
  return shopifyAPI
    .productImage
    .list(productId)
    .catch(e => []);
}

