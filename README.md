# About
* An application that unifies data fetching for the Magnolia shopping experience into a single Graphql API.

# Setup
* Create a `shopify-config.json` in the format of `shopify-config.example.json`.

# Commands
* Install dependencies: `npm install`
* Start graphql server: `npm run start`

