// This module is responsible for exposing the data layer
// with a graphql server

const { ApolloServer, gql } = require('apollo-server');

const { constructStorageSuite } = require('./shopify-storage/implementation');

// Read storage config
const fs = require('fs');
const configString= fs.readFileSync('shopify-config.json', 'utf8');
const config = JSON.parse(configString);

// Construct Storage Suite
const {
  Collections,
  CollectionMedia,
  Products,
  Variants,
  VariantMetaFields,
  ProductMetaFields,
  ProductMedia,
  VariantMedia,
} = constructStorageSuite(config); 

// Graphql Type Definitions
const typeDefs = gql`
  type Query {
    collection(id:String!): Collection,

    collections(
      limit: Int,
      condition: CollectionCondition,
    ): [Collection]

    product(id:String!): Product,

    products(
      limit: Int,
      condition: ProductCondition,
    ): [Product]

    variant(id:String!): Variant,
  }

  input CollectionCondition {
    title: String
    slug: String
    isPrivate: Boolean
  }

  input ProductCondition {
    title: String
    slug: String
    isPrivate: Boolean
  }

  type Collection {
    id: String!
    title: String!
    slug: String!
    isPrivate: Boolean!
    media: [Media]!
    products: [Product]!
  }

  type Product {
    id: String!
    title: String!
    slug: String!
    description: String!
    isPrivate: Boolean!
    variants: [Variant]!
    metafields: [MetaField]!
    media: [Media]!
  }

  type Variant {
    id: String!
    title: String!
    quantityAvailable: Int!
    priceInCents: Int!
    weightInGrams: Int!
    metafields: [MetaField]!
    media: [Media]!
  }

  type MetaField {
    key: String! 
    value: String! 
    isPrivate: Boolean! 
  }

  type Media {
    url: String!
    format: String!
    description: String
    widthInPixels: Int!
    heightInPixels: Int!
  }
`
const resolvers = {
  Query: {
    collection: (parent, args, context, info) => {
      return Collections.findById(args.id);
    },
    collections: (parent, args, context, info) => {
      return Collections.find({
        limit: args.limit,
        where: args.condition,
      });
    },
    product: (parent, args, context, info) => {
      return Products.findById(args.id);
    },
    products: (parent, args, context, info) => {
      return Products.find({
        limit: args.limit,
        where: args.condition,
      });
    },
    variant: (parent, args, context, info) => {
      return Variants.findById(args.id);
    },
  },
  Collection: {
    media(collection) {
      return CollectionMedia.findByCollectionId(collection.id);
    },
    products(collection) {
      return Products.findByCollectionId(collection.id);
    },
  },
  Product: {
    media(product) {
      return ProductMedia.findByProductId(product.id);
    },
    metafields(product) {
      return ProductMetaFields.findByProductId(product.id);
    },
    variants(product) {
      return Variants.findByProductId(product.id);
    },
  },
  Variant: {
    media(variant) {
      return VariantMedia.findByVariantId(variant.id);
    },
    metafields(variant) {
      return VariantMetaFields.findByVariantId(variant.id);
    },
  },

}

const server = new ApolloServer({ typeDefs, resolvers });

server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});

