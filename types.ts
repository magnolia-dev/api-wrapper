
type Primitive = boolean | string | number | null;

// An object that can perform CRUD operations on
// one particular data type.
export interface StorageDevice {
  find?: (options?: FindOptions) => Promise<Instance[]>,
  findById?: (id:string) => Promise<Instance|undefined>,
}

export interface CollectionStorage extends StorageDevice {
  find: (options?: FindOptions) => Promise<CollectionInstance[]>,
  findById: (id:string) => Promise<CollectionInstance|undefined>,
  findByProductId: (productId:string, options?: FindOptions) => Promise<CollectionInstance[]>,
}

export interface ProductStorage extends StorageDevice {
  find: (options?: FindOptions) => Promise<ProductInstance[]>,
  findById: (id:string) => Promise<ProductInstance|undefined>,
  findByCollectionId: (collectionId:string, options?: FindOptions) => Promise<ProductInstance[]>,
}

// The shopify wrapper library we're using doesn't provide
// a way to get a list of variants without a product id.
// Although the use case isn't really there, it might be
// good to provide it for uniformity's sake.
export interface VariantStorage extends StorageDevice {
  findById: (id:string) => Promise<VariantInstance|undefined>,
  findByProductId: (productId:string, options?: FindOptions) => Promise<VariantInstance[]>,
}

export interface VariantMetaFieldStorage extends StorageDevice {
  findByVariantId: (variantId:string) => Promise<MetaFieldInstance[]>,
}

export interface ProductMetaFieldStorage extends StorageDevice {
  findByProductId: (productId:string) => Promise<MetaFieldInstance[]>,
}

export interface CollectionMediaStorage extends StorageDevice {
  findByCollectionId: (collectionId:string) => Promise<MediaInstance[]>,
}

export interface ProductMediaStorage extends StorageDevice {
  findByProductId: (productId:string) => Promise<MediaInstance[]>,
}

export interface VariantMediaStorage extends StorageDevice {
  findByVariantId: (variantId:string) => Promise<MediaInstance[]>,
}

export interface StorageSuiteConfig {
  [key:string]: any;
}

// A factory function that can be used to create a
// useable StorageSuite instance
export type StorageSuiteConstructor =
  (config: StorageSuiteConfig) => StorageSuite;

// A group of storage devices that can be used to access
// any type of persisted data type used in the application
export interface StorageSuite {
  Collections: CollectionStorage,
  CollectionMedia: CollectionMediaStorage,
  Products: ProductStorage,
  Variants: VariantStorage,
  ProductMetaFields: ProductMetaFieldStorage,
  VariantMetaFields: VariantMetaFieldStorage,
  ProductMedia: ProductMediaStorage,
  VariantMedia: VariantMediaStorage,
}

export interface Instance {
  readonly id?: string,
}

// Options that specify how to retrieve persisted
// objects from a StorageDevice
export interface FindOptions {
  where?: { [k:string]: Primitive },
  limit?: number,
}

export interface CollectionInstance extends Instance {
  title: string,
  slug: string,
  isPrivate: boolean,
}

export interface ProductInstance extends Instance {
  title: string,
  slug: string,
  description: string,
  isPrivate: boolean,
}

export interface VariantInstance extends Instance {
  title: string,
  quantityAvailable: number,
  priceInCents: number,
  weightInGrams: number,
  variesBy?: string,
}

export interface MetaFieldInstance extends Instance {
  key: string,
  value: string,
  isPrivate: boolean,
}

export interface MediaInstance extends Instance {
  url: string,
  format: string,
  description: string,
  widthInPixels: number,
  heightInPixels: number,
}

export interface ProductReviewInstance extends Instance {
  // Fields
  stars: number, // Value from 1 to 5 that indicates the customer's satisfaction
  author: string, // The name that the customer chooses to identify themself
  email: string, // An email address that the customer can be contacted at
  title: string, // A brief summary of the review
  message: string, // A customer's full description of their experience
  reply: string | null, // A response to the review by an admin
  isPublished: boolean, // Whether or not the review has been approved for public display
  createdDate: string,
}

// Some merits of returning entities wrapped in the Node class (ProductsConnection): 
// * You can separate entity data from meta like totalCount, pageInfo (for pagination), edges
//
// What determines whether an entity should be wrapped with the Node interface?

